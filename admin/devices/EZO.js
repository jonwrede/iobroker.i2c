﻿function EZO(name, baseAddress) {
    DeviceBase.call(this, name);
    this.addresses = baseAddress;
    this.name = name;

    this.template = '<tr><td><label for="{{:#parent.parent.data.address}}-pollingInterval">{{t:"Polling Interval (ms)"}}</label></td><td class="admin-icon"></td>';
    this.template += '<td><input type="text" id="{{:#parent.parent.data.address}}-pollingInterval" data-link="EZO.pollingInterval" /></td></tr>';
    switch (name) {
        case 'EZO_EC':
            this.template += '<tr><td><label for="{{:#parent.parent.data.address}}-dry">{{t:"Dry Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><button id="{{:#parent.parent.data.address}}-dry-button" data-link="{on EZO.calibrateDry}">Calibrate</button></td></tr>'
                + '<tr><td><label for="{{:#parent.parent.data.address}}-low">{{t:"Low Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><input type="text" id="{{:#parent.parent.data.address}}-low" data-link="EZO.low" />'
                + '<button id="{{:#parent.parent.data.address}}-low-button" data-link="{on EZO.calibrateLow}">Calibrate</button></td></tr>'
                + '<tr><td><label for="{{:#parent.parent.data.address}}-high">{{t:"High Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><input type="text" id="{{:#parent.parent.data.address}}-high" data-link="EZO.high" />'
                + '<button id="{{:#parent.parent.data.address}}-high-button" data-link="{on EZO.calibrateHigh}">Calibrate</button></td></tr>'
                + '<tr><td>Output</td><td></td><td>'
                + '<label>EC</label><input type="checkbox" id="{{:#parent.parent.data.address}}-EC" data-link="EZO.output.EC">'
                + '<label>TDS</label><input type="checkbox" id="{{:#parent.parent.data.address}}-TDS"  data-link="EZO.output.TDS">'
                + '<label>S</label><input type="checkbox" id="{{:#parent.parent.data.address}}-S"  data-link="EZO.output.S">'
                + '<label>SG</label><input type="checkbox" id="{{:#parent.parent.data.address}}-SG" data-link="EZO.output.SG">'
                + '<button id="{{:#parent.parent.data.address}}-setout-button" data-link="{on EZO.setOutput}">Enable</button><button id="{{:#parent.parent.data.address}}-getout-button" data-link="{on EZO.getOutput}">Check</button></td></tr>';

            break;

        case 'EZO_PH':
            this.template += '<tr><td><label for="{{:#parent.parent.data.address}}-mid">{{t:"Mid Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><input type="text" id="{{:#parent.parent.data.address}}-mid" data-link="EZO.mid" /></td>'
                + '<td><button id="{{:#parent.parent.data.address}}-mid-button" data-link="{on EZO.calibrateMid}">Calibrate</button></td></tr>'
                + '<tr><td><label for="{{:#parent.parent.data.address}}-low">{{t:"Low Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><input type="text" id="{{:#parent.parent.data.address}}-low" data-link="EZO.low" /></td>'
                + '<td><button id="{{:#parent.parent.data.address}}-low-button" data-link="{on EZO.calibrateLow}">Calibrate</button></td></tr>'
                + '<tr><td><label for="{{:#parent.parent.data.address}}-high">{{t:"High Point"}}</label></td><td class="admin-icon"></td>'
                + '<td><input type="text" id="{{:#parent.parent.data.address}}-high" data-link="EZO.high" /></td>'
                + '<td><button id="{{:#parent.parent.data.address}}-high-button" data-link="{on EZO.calibrateHigh}">Calibrate</button></td></tr>';

            break;
    }
    this.template += '<tr><td id="{{:#parent.parent.data.address}}-calibrated"></td><td>'
        + '<button id="{{:#parent.parent.data.address}}-calibrated-button" data-link="{on EZO.calibrated}">Check</button></td>'
        + '<td><button id="{{:#parent.parent.data.address}}-clear-button" data-link="{on EZO.calibrateClear}">Clear</button></td></tr>'
}

EZO.prototype = Object.create(DeviceBase.prototype);
EZO.prototype.constructor = EZO;

EZO.prototype.prepareViewModel = function (device) {
    device.EZO = device.EZO || {};
    device.EZO.pollingInterval = device.EZO.pollingInterval || 2000;
    switch (this.name) {
        case 'EZO_PH':
            device.EZO.low = device.EZO.low || 4;
            device.EZO.mid = device.EZO.mid || 7;
            device.EZO.high = device.EZO.high || 10;
            break;

        case 'EZO_EC':
            device.EZO.low = device.EZO.low || 12880;
            device.EZO.high = device.EZO.high || 80000;
            device.EZO.output = { 'EC': false , 'TDS': false , 'S': false, 'SG': false };
            break;
    }
    device.EZO.send = this.send;
    device.EZO.calibrated = this.calibrated;
    device.EZO.calibrateDry = this.calibrateDry;
    device.EZO.calibrateLow = this.calibrateLow;
    device.EZO.calibrateMid = this.calibrateMid;
    device.EZO.calibrateHigh = this.calibrateHigh;
    device.EZO.calibrateHigh = this.calibrateHigh;
    device.EZO.calibrateClear = this.calibrateClear;
    device.EZO.setOutput = this.setOutput;
    device.EZO.getOutput = this.getOutput;
    device.EZO.address = device.address;
    return device;
};

EZO.prototype.prepareModel = function (device) {
    device.name = this.name;
    return device;
};

EZO.prototype.setOutput = function () {
    this.send('set_out', this.output, function (data) {});
};
EZO.prototype.getOutput = function () {
    var that = this;
    this.send('get_out', '', function (data) {
        for (var k in that.output) {
            $('#'+ that.address +'-'+k).prop('checked',false);
            that.output[k] = false;
        }
        data.split(',').forEach(function (data) {
            if (that.output.hasOwnProperty(data)) {
                $('#'+ that.address +'-'+data).prop('checked',true);
                that.output[data] = true;
            }
        })
    });
};

EZO.prototype.calibrated = function () {
    var that = this;
    this.send('cal_?','', function (data) {
        if (data.split(',')[1] === '2') {
            $('#'+ that.address +'-calibrated').css("background-color", "#4CAF50").html("Calibrated");
        } else if (data.split(',')[1] === '0') {
            $('#'+ that.address +'-calibrated').css("background-color", "#e7e7e7").html("Not calibrated");
        }
    })
};

EZO.prototype.calibrateLow = function () {
    var that = this;
    $('#'+ that.address +'-low-button').prop("disabled",true);
    setTimeout(function () {
        that.send('cal_low', that.low, function (data) {
            $('#'+ that.address +'-low-button').prop("disabled",false);
        })
    }, 5000);
};

EZO.prototype.calibrateClear = function () {
    var that = this;
    this.send('cal_clear', '', function (data) {
        $('#'+ that.address +'-calibrated').css("background-color", "#e7e7e7").html("Not calibrated");
    })
};

EZO.prototype.calibrateMid = function () {
    var that = this;
    $('#'+ that.address +'-mid-button').prop("disabled",true);
    setTimeout(function () {
        that.send('cal_mid', that.mid, function (data) {
            $('#'+ that.address +'-mid-button').prop("disabled",false);
        })
    }, 5000);
};

EZO.prototype.calibrateHigh = function () {
    var that = this;
    $('#'+ that.address +'-high-button').prop("disabled",true);
    setTimeout(function () {
        that.send('cal_high', that.high, function (data) {
            $('#'+ that.address +'-high-button').prop("disabled",false);
        })
    }, 5000);
};

EZO.prototype.calibrateDry = function () {
    var that = this;
    $('#'+ that.address +'-dry-button').prop("disabled",true);
    setTimeout(function () {
        that.send('cal_dry', '', function (data) {
            $('#'+ that.address +'-dry-button').prop("disabled",false);
        })
    }, 5000);
};

EZO.prototype.send = function (command, message, callback) {
    sendTo('i2c.' + instance, 'device', { address: this.address, command: command, message: message}, function (data) {
        callback(data.replace(/"/g,''))
    });
};

// "exports" of all device types supported by this class
deviceTypes.EZO_EC = new EZO('EZO_EC', [0x69, 0x6B, 0x64]);
deviceTypes.EZO_PH = new EZO('EZO_PH', [0x63, 0x6C, 0x6A]);

// translations
systemDictionary['Polling Interval (ms)'] = {
    "en": "Polling Interval (ms)",
    "de": "Abfrage-Intervall (ms)"
};
systemDictionary['Lower Point'] = {
    "en": "Lower Point",
    "de": "Unterer Punkt"
};
systemDictionary['Middle Point'] = {
    "en": "Middle Point",
    "de": "Mittlerer Punkt"
};
systemDictionary['Upper Point'] = {
    "en": "Upper Point",
    "de": "Höherer Punkt"
};
