﻿"use strict";

function create(deviceConfig, i2cAdapter) {
    return new EZO(deviceConfig, i2cAdapter);
}

function EZO(deviceConfig, i2cAdapter) {
    this.address = deviceConfig.address;
    this.name = deviceConfig.name || 'EZO';
    this.hexAddress = i2cAdapter.toHexString(this.address);
    this.longInterval = 900;
    this.shortInterval = 300;
    this.config = deviceConfig.EZO;
    this.i2cAdapter = i2cAdapter;
    this.adapter = this.i2cAdapter.adapter;
}

EZO.prototype.start = function () {
    var that = this;
    that.debug('Starting');
    that.adapter.setObject(that.hexAddress, {
        type: 'device',
        common: {
            name: this.name,
            role: 'sensor'
        },
        native: {
            pollingInterval: that.config.pollingInterval,
        }
    });
    if (that.name === 'EZO_EC'){
        this.enabledOutputs(function (data) {
            that.sendCommand('O,?', that.shortInterval, function (data) {
                that.debug(data);
                var ordering = data.split(',').slice(1);
                for (var i = 0; i < ordering.length; i++) {
                    that.createState(ordering[i], i);
                }
            });
        });
    } else if (that.name === 'EZO_PH') {
        that.createState('PH', 0)
    }
    if (that.config.pollingInterval && parseInt(that.config.pollingInterval) > 0) {
        this.pollingTimer = setInterval(
            function () {
                that.readValues(function (data) {

                });
            }, Math.max(2000, parseInt(that.config.pollingInterval)));
    }
};

EZO.prototype.stop = function () {
    this.debug('Stopping');
    clearInterval(this.pollingTimer);
};

EZO.prototype.debug = function (message) {
    this.adapter.log.info('EZO ' + this.address + ': ' + message);
};

EZO.prototype.info = function(cb) {
    this.sendCommand('I', this.shortInterval, cb);
};

EZO.prototype.readValues = function(cb) {
    var that = this;
    this.sendCommand('R', this.longInterval, function (data) {
        var ordering = data.split(',');
        for (var i = 0; i < ordering.length; i++) {
            that.setStateAck(i,ordering[i]);
        }
        cb(data);
    });
};
EZO.prototype.convert = function(value) {
   if (value === true) {
       return 1;
   } else {
       return 0;
   }
};

EZO.prototype.setOutputs = function(value, cb) {
    var that = this;
    if (that.name === 'EZO_EC') {
        that.sendCommand('O,EC,'+that.convert(value['EC']),that.shortInterval, function () {
            that.sendCommand('O,TDS,'+that.convert(value['TDS']),that.shortInterval, function () {
                that.sendCommand('O,S,'+that.convert(value['S']),that.shortInterval, function () {
                    that.sendCommand('O,SG,'+that.convert(value['SG']),that.shortInterval, function () {
                        that.enabledOutputs(function () {
                        })
                    })
                })
            })
        })
    }
};

EZO.prototype.enabledOutputs = function(cb) {
    var that = this;
    if (that.name === 'EZO_EC'){
        that.sendCommand('O,?', this.shortInterval, function (data) {
            cb(data);
        });
    }
};

EZO.prototype.calibrateDry = function(cb) {
    if (this.name === 'EZO_EC') {
        this.sendCommand('Cal,dry', this.longInterval, cb);
    }
};

EZO.prototype.calibrateLow = function(value, cb) {
    this.sendCommand('Cal,low,' + value, this.longInterval, cb);
};

EZO.prototype.calibrateMid = function(value, cb) {
    if (this.name === 'EZO_PH') {
        this.sendCommand('Cal,mid,' + value, this.longInterval, cb);
    }
};

EZO.prototype.calibrateHigh = function(value, cb) {
    this.sendCommand('Cal,high,'+ value, this.longInterval, cb);
};

EZO.prototype.calibrateClear = function(cb) {
    this.sendCommand('Cal,clear', this.short, cb);
};

EZO.prototype.calibrated = function(cb) {
    this.sendCommand('Cal,?', this.shortInterval, cb);
};

EZO.prototype.setProbe = function(value, cb) {
    var that = this;
    if (that.name === 'EZO_EC') {
        that.sendCommand('K,' + value, this.shortInterval, cb);
    }
};

EZO.prototype.getProbe = function(cb) {
    var that = this;
    if (that.name === 'EZO_EC') {
        that.sendCommand('K,?', this.longInterval, cb);
    }
};

EZO.prototype.error = function (message) {
    this.adapter.log.error('EZO ' + this.address + ': ' + message);
};

EZO.prototype.createState = function (value, order) {
    this.adapter.setObject(this.hexAddress + '.' + order, {
        type: 'state',
        common: {
            name: value,
            read: true,
            write: false,
            type: 'number',
            role: 'state',
        },
        native: {

        }
    });
};

EZO.prototype.setStateAck = function (pin, value) {
    return this.i2cAdapter.setStateAck(this.hexAddress + '.' + pin, value);
};

EZO.prototype.getStateValue = function (pin) {
    return this.i2cAdapter.getStateValue(this.hexAddress + '.' + pin);
};

EZO.prototype.sendCommand = function (command, duration, cb) {
    var that = this;
    that.debug('[' + this.address + ']: ' + command);
    var sendBuffer = new Buffer(command);
    that.i2cAdapter.bus.i2cWriteSync(that.address, command.length, sendBuffer);
    setTimeout(function () {
        var outputBuffer = new Buffer(32);
        that.i2cAdapter.bus.i2cReadSync(that.address, 32, outputBuffer);
        cb(outputBuffer.toString('ascii').replace(/\u0000/g,'').replace(/\u0001/g,''));
    }.bind(this), duration);
};

EZO.prototype.onMessage = function (command, message, callback) {
    var that = this;
    switch (command) {
        case 'cal_?':
            that.calibrated(callback);
            break;
        case 'cal_clear':
            that.calibrateClear(callback);
            break;
        case 'cal_dry':
            that.calibrateDry(callback);
            break;
        case 'cal_low':
            that.calibrateLow(message, callback);
            break;
        case 'cal_mid':
            that.calibrateMid(message, callback);
            break;
        case 'cal_high':
            that.calibrateHigh(message, callback);
            break;
        case 'set_out':
            that.setOutputs(message, callback);
            break;
        case 'get_out':
            that.enabledOutputs(callback);
            break;
    }
};

module.exports.create = create;